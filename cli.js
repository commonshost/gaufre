#!/usr/bin/env node

const { createServer } = require('http')
const { once } = require('events')
const { createReadStream } = require('fs')
const { join } = require('path')

const fileMap = {
  '/manifest.json': ['manifest.json', 'application/json'],
  '/service-worker.js': ['service-worker.js', 'application/javascript'],
  '/favicon.ico': ['favicon.ico', 'image/vnd.microsoft.icon'],
  '/icon.png': ['icon.png', 'image/png'],
  '/index.html': ['index.html', 'text/html']
}

async function main () {
  const server = createServer((request, response) => {
    console.log(request.method, request.url)
    const [path, type] = fileMap[request.url] || fileMap['/index.html']
    response.setHeader('Content-Type', type)
    const filepath = join(__dirname, 'public', path)
    const file = createReadStream(filepath)
    file.pipe(response)
  })
  server.listen(process.argv[2] || 3000)
  await once(server, 'listening')
  const { port } = server.address()
  console.log(`Listening on http://localhost:${port}`)
}

main()
